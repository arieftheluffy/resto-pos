@extends('layouts.template')

@section('css')
  <!-- Page plugins -->
  <link rel="stylesheet" href="{{ ('assets/vendor/datatables/dataTables.bootstrap4.min.css') }}">
  <link rel="stylesheet" href="{{ ('assets/vendor/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css') }}">
  {{-- <link rel="stylesheet" href="{{ ('assets/vendor/datatables/select.bootstrap4.min.css') }}"> --}}
@endsection

@section('content')
<!-- Header -->
<div class="header bg-primary pb-6">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
                <div class="col-lg-6 col-7">
                    <h6 class="h2 text-white d-inline-block mb-0">Setting</h6>
                    <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                        <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                            <li class="breadcrumb-item"><a href="/dashboard"><i class="fas fa-home"></i></a></li>
                            <li class="breadcrumb-item"><a href="#">Setting</a></li>
                            <li class="breadcrumb-item"><a href="#">Menu Makanan</a></li>
                        </ol>
                    </nav>
                </div>

            </div>
        </div>
    </div>
</div>
<!-- Page content -->
<div class="container-fluid mt--6">
  <div class="row">
      <div class="col-lg-6">
        <div class="card">
          <!-- Card header -->
          <div class="card-header">
            <div class="text-left">
              <button type="button" class="btn btn-md btn-default" data-toggle="modal" data-target="#modal-default">Tambah Daftar Menu</button>
            </div>
          </div>
          <div class="table-responsive py-4">
            <table class="table table-flush" id="datatable-menu">
              <thead class="thead-light">
                <tr>
                  <th>No</th>
                  <th>Daftar Menu</th>
                  <th>Harga per Porsi</th>
                  <th>Kategori</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                  <th>No</th>
                  <th>Daftar Menu</th>
                  <th>Harga per Porsi</th>
                  <th>Kategori</th>
                  <th>Aksi</th>
                </tr>
              </tfoot>
              <tbody>
                @foreach($daftar_menu as $data)
                  <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $data->nama_menu }}</td>
                    <td>Rp. {{ $data->harga }}</td>
                    <td>{{ $data->kategori->nama_kategori }}</td>
                    <td>
                      <a href="#" class="btn btn-sm btn-success"
                      data-toggle="modal"
                      data-target="#modal-edit"
                      data-menu_id="{{$data->menu_id}}"
                      data-nama_menu="{{$data->nama_menu}}"
                      data-harga="{{$data->harga}}"
                      data-kategori_id="{{$data->kategori->kategori_id}}"
                      data-ket="{{$data->ket}}"><i class='fa fa-pencil'></i></a>
                      <a href="#" class="btn btn-sm btn-danger confirm-delete" data-id={{ $data->menu_id }}><i class="fa fa-trash"></i>
                      <form action="{{ url('/menu/delete', $data->menu_id)}}" id="delete{{ $data->menu_id }}" method="POST">
                        @csrf
                        @method('delete')
                      </form></a>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="col-lg-6">
        <div class="card">
          <!-- Card header -->
          <div class="card-header">
            {{-- <h3 class="mb-0">Menu Makanan</h3> --}}
          <div class="text-left">
              <button type="button" class="btn btn-md btn-neutral" data-toggle="modal" data-target="#modal-kategori">Tambah Kategori</button>
          </div>
          </div>
          <div class="table-responsive py-4">
            <table class="table align-items-center table-light table-flush" id="datatable-kategori">
              <thead class="thead-light">
                <tr>
                  <th>No</th>
                  <th>Nama Kategori</th>
                  <th>Keterangan</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                  <th>No</th>
                  <th>Nama Kategori</th>
                  <th>Keterangan</th>
                  <th>Aksi</th>
                </tr>
              </tfoot>
              <tbody>
                @foreach($data_kategori as $datakat)
                  <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $datakat->nama_kategori }}</td>
                    <td>{{ $datakat->ket }}</td>
                    <td>
                      <a href="#" class="btn btn-sm btn-success"
                      data-toggle="modal"
                      data-target="#modal-editkategori"
                      data-kategori_id="{{$datakat->kategori_id}}"
                      data-nama_kategori="{{$datakat->nama_kategori}}"
                      data-ket="{{$datakat->ket}}"><i class='fa fa-pencil'></i></a>
                      <a href="#" class="btn btn-sm btn-danger confirm-delete-kategori" data-id="{{$datakat->kategori_id}}"><i class="fa fa-trash"></i>
                      <form action="{{ url('/menu/kategori/delete', $datakat->kategori_id)}}" id="delete{{ $datakat->kategori_id }}" method="POST">
                        @method('delete')
                        @csrf
                      </form></a>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
  </div>
</div>

<!-- ModalTambahData -->
<div class="modal fade" id="modal-default" tabindex="-1" role="dialog" aria-labelledby="modal-default" aria-hidden="true">
  <div class="modal-dialog modal- modal-dialog-centered modal-" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h6 class="modal-title" id="modal-title-default">Tambah Data</h6>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" method="post" action="{{ url('/menu/simpan') }}">
          @csrf
          <div class="form-group">
            <label class="form-control-label" for="nama_menu">Nama Makanan/Minuman<span><strong> *</strong></span></label>
            <input type="text" class="form-control" id="nama_menu" name="nama_menu" placeholder="Nama Makanan/Minuman" required>
          </div>
          <div class="form-group">
            <label class="form-control-label" for="kategori_id">Kategori</label>
            <select class="form-control" id="kategori_id" name="kategori_id">
            @foreach($data_kategori as $kat)
              <option value="{{ $kat->kategori_id }}">{{ $kat->nama_kategori }}</option>
            @endforeach
            </select>
          </div>
          <div class="form-group">
            <label class="form-control-label" for="harga">Harga Makanan/Minuman</label>
            <input type="number" class="form-control" id="harga" placeholder="Harga Makanan (dalam rupiah)" name="harga" required>
          </div>
          <div class="form-group">
            <label class="form-control-label" for="ket">Keterangan</label>
            <textarea class="form-control" id="ket" rows="3" name="ket"></textarea>
          </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Save</button>
        <button type="button" class="btn btn-link ml-auto" data-dismiss="modal">Close</button>
      </div>
      </form>
    </div>
  </div>
</div>

<!-- ModalEditData -->
<div class="modal fade" id="modal-edit" tabindex="-1" role="dialog" aria-labelledby="modal-edit" aria-hidden="true">
  <div class="modal-dialog modal- modal-dialog-centered modal-" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h6 class="modal-title" id="modal-title-edit">Edit Data</h6>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" method="post" action="{{ url('/menu/update') }}">
          @method('patch')
          @csrf
          <input type='hidden' name='menu_id' class='modal_hiddenid' id="menu_id">
          <div class="form-group">
            <label class="form-control-label" for="nama_menu">Nama Makanan/Minuman</label>
            <input type="text" class="form-control" id="nama_menu" name="nama_menu" placeholder="Nama Makanan/Minuman" required>
          </div>
          <div class="form-group">
            <label class="form-control-label" for="kategori_id">Kategori</label>
            <select class="form-control" id="kategori_id" name="kategori_id">
            @foreach($data_kategori as $kat)
              <option value="{{ $kat->kategori_id }}">{{ $kat->nama_kategori }}</option>
            @endforeach
            </select>
          </div>
          <div class="form-group">
            <label class="form-control-label" for="harga">Harga Makanan/Minuman</label>
            <input type="number" class="form-control" id="harga" placeholder="Harga Makanan (dalam rupiah)" name="harga" required>
          </div>
          <div class="form-group">
            <label class="form-control-label" for="ket">Keterangan</label>
            <textarea class="form-control" id="ket" rows="3" name="ket"></textarea>
          </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Update</button>
        <button type="button" class="btn btn-link ml-auto" data-dismiss="modal">Close</button>
      </div>
      </form>
    </div>
  </div>
</div>

<!-- ModalTambahData Kategori -->
<div class="modal fade" id="modal-kategori" tabindex="-1" role="dialog" aria-labelledby="modal-kategori" aria-hidden="true">
  <div class="modal-dialog modal- modal-dialog-centered modal-" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h6 class="modal-title" id="modal-title-kategori">Tambah Data</h6>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" method="post" action="{{ url('/menu/kategori/simpan') }}">
          @csrf
          <div class="form-group">
            <label class="form-control-label" for="nama_kategori">Nama Kategori<span><strong> *</strong></span></label>
            <input type="text" class="form-control" id="nama_kategori" name="nama_kategori" placeholder="Nama Kategori" required>
          </div>
          <div class="form-group">
            <label class="form-control-label" for="ket">Keterangan</label>
            <textarea class="form-control" id="ket" rows="3" name="ket"></textarea>
          </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary" data-toggle="sweet-alert" data-sweet-alert="success">Simpan</button>
        <button type="button" class="btn btn-link ml-auto" data-dismiss="modal">Close</button>
      </div>
      </form>
    </div>
  </div>
</div>

<!-- ModalEditData Kategori-->
<div class="modal fade" id="modal-editkategori" tabindex="-1" role="dialog" aria-labelledby="modal-editkategori" aria-hidden="true">
  <div class="modal-dialog modal- modal-dialog-centered modal-" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h6 class="modal-title" id="modal-title-editkategori">Edit Kategori</h6>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" method="post" action="{{ url('/menu/kategori/update') }}">
          @method('patch')
          @csrf
          <input type='hidden' name='kategori_id' class='modal_hiddenid' id="kategori_id">
          <div class="form-group">
            <label class="form-control-label" for="nama_kategori">Nama Kategori</label>
            <input type="text" class="form-control" id="nama_kategori" name="nama_kategori" placeholder="Nama Kategori" required>
          </div>
          <div class="form-group">
            <label class="form-control-label" for="ket">Keterangan</label>
            <textarea class="form-control" id="ket" rows="3" name="ket"></textarea>
          </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Update</button>
        <button type="button" class="btn btn-link ml-auto" data-dismiss="modal">Close</button>
      </div>
      </form>
    </div>
  </div>
</div>
@endsection

@section('js')
    <!-- editData -->
     <script>
        $('#modal-edit').on('show.bs.modal', function (event) {

          var button = $(event.relatedTarget) // Button that triggered the modal
          var menu_id    = button.data('menu_id')
          var nama_menu    = button.data('nama_menu')
          var kategori_id    = button.data('kategori_id')
          var ket = button.data('ket')
          var harga = button.data('harga')
          var modal = $(this)

          modal.find('.modal-body #menu_id').val(menu_id);
          modal.find('.modal-body #nama_menu').val(nama_menu);
          modal.find('.modal-body #kategori_id').val(kategori_id);
          modal.find('.modal-body #ket').val(ket);
          modal.find('.modal-body #harga').val(harga);
        })

        $('#modal-editkategori').on('show.bs.modal', function (event) {

          var button = $(event.relatedTarget) // Button that triggered the modal
          var kategori_id    = button.data('kategori_id')
          var nama_kategori    = button.data('nama_kategori')
          var ket = button.data('ket')
          var modal = $(this)

          modal.find('.modal-body #kategori_id').val(kategori_id);
          modal.find('.modal-body #nama_kategori').val(nama_kategori);
          modal.find('.modal-body #ket').val(ket);
        })
      </script>
    <!-- Optional JS -->
    <script src="{{ ('assets/vendor/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ ('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
    {{-- <script src="{{ ('assets/vendor/datatables.net/js/jquery.dataTables.min.js') }}"></script> --}}
    {{-- <script src="{{ ('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script> --}}
    <script src="{{ ('assets/vendor/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ ('assets/vendor/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ ('assets/vendor/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ ('assets/vendor/datatables.net-buttons/js/buttons.flash.min.js') }}"></script>
    <script src="{{ ('assets/vendor/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
    {{-- <script src="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css"></script> --}}

      <!-- Optional JS -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script src="{{ ('assets/vendor/bootstrap-notify/bootstrap-notify.min.js') }}"></script>
    <script>
    $(".confirm-delete").click(function(e) {
      id = e.target.dataset.id;
      Swal.fire({
        title: 'Yakin data akan dihapus?',
        text: "Data akan dihapus permanen!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Hapus!'
      }).then((result) => {
        if (result.value) {
          Swal.fire({
            icon: 'success',
            title: 'Berhasil dihapus',
            showConfirmButton: false,
            timer: 1500
          })
          $(`#delete${id}`).submit();
        }
      })
    });

    $(".confirm-delete-kategori").click(function(e) {
      id = e.target.dataset.id;
      Swal.fire({
        title: 'Yakin data akan dihapus? ',
        text: "Data akan dihapus permanen!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Hapus!'
      }).then((result) => {
        if (result.value) {
          $(`#delete${id}`).submit();
          Swal.fire({
            icon: 'success',
            title: 'Berhasil dihapus',
            showConfirmButton: false,
            timer: 1500
          })
        }
      })
    });

    $(document).ready( function () {
        $('#datatable-menu').DataTable();
        $('#datatable-kategori').DataTable();
    } );
</script>
@endsection
