<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/dashboard', 'HomeController@index')->name('home');
Route::get('/menu', 'MenuMakananController@index');
Route::post('/menu/simpan', 'MenuMakananController@simpan');
Route::patch('/menu/update', 'MenuMakananController@update');
Route::delete('/menu/delete/{id}', 'MenuMakananController@delete');

Route::post('menu/kategori/simpan', 'KategoriController@simpan');
Route::patch('menu/kategori/update', 'KategoriController@update');
Route::delete('menu/kategori/delete/{id}', 'KategoriController@delete');
