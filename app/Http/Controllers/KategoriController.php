<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\kategori;
use DB;
use Validator;

class KategoriController extends Controller
{
    public function simpan(Request $request)
    {
        $input = $request->all();
        // return $input;

        $validasi = Validator::make($input, [
            'nama_kategori' => 'required|min:4|max:200',
            'ket' => 'sometimes|max:250',
        ]);

        if ($validasi->fails()) {
            return back()->with('warning', $validasi->messages()->all()[0])->withInput();
        }

        kategori::create($input);
        return redirect()->back();
    }

    public function update(Request $request)
    {
        $input = $request->all();
        return $input;

        $validasi = Validator::make($input, [
            'nama_kategori' => 'required|min:4|max:200',
            'ket'           => 'sometimes|max:250',
        ]);

        if ($validasi->fails()) {
            return back()->with('warning', $validasi->messages()->all()[0])->withInput();
        }

        $kategori = kategori::findOrFail($request->kategori_id);
        $kategori->update($input);
        return redirect()->back();
    }

    public function delete($id)
    {
        // return $id;
        DB::table('kategori')->where('kategori_id', $id)->delete();
        return redirect()->back();
    }
}
