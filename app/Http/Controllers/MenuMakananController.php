<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\DaftarMenu;
use App\kategori;
use DB;

class MenuMakananController extends Controller
{
    public function index()
    {
        $data_kategori = kategori::all();
        // return $data_kategori;
        $daftar_menu = DaftarMenu::all();
        return view('menu', compact('data_kategori', 'daftar_menu'));
    }

    public function simpan(Request $request)
    {
        $input = $request->all();
        // return $input;

        $validasi = Validator::make($input, [
            'nama_menu' => 'required|min:4|max:200',
            'harga' => 'required|min:3000|max:25000|numeric',
            'ket' => 'sometimes|max:250',
            'kategori_id' => 'required',
        ]);

        if ($validasi->fails()) {
            return back()->with('warning', $validasi->messages()->all()[0])->withInput();
        }

        DaftarMenu::create($input);
        return redirect()->back();
    }

    public function update(Request $request)
    {
        $input = $request->all();
        // return $input;

        $validasi = Validator::make($input, [
            'nama_menu' => 'required|min:4|max:200',
            'harga' => 'required|min:3000|max:25000|numeric',
            'ket' => 'sometimes|max:250',
            'kategori_id' => 'required',
        ]);

        if ($validasi->fails()) {
            return back()->with('warning', $validasi->messages()->all()[0])->withInput();
        }

        $daftar_menu = DaftarMenu::findOrFail($request->menu_id);
        $daftar_menu->update($input);
        return redirect()->back();
    }

    public function delete($id)
    {
        // return $id;
        DB::table('menu')->where('menu_id', $id)->delete();
        return redirect()->back();
    }
}
