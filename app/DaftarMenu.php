<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DaftarMenu extends Model
{
    protected $table = 'menu';
    protected $primaryKey = 'menu_id';
    protected $fillable = [
        'menu_id','nama_menu', 'kategori_id', 'harga', 'ket'
    ];

    public function kategori()
    {
        return $this->belongsTo('App\kategori', 'kategori_id');
    }
}
